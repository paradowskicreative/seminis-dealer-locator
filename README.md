# seminis-dealer-locator

This project uses:

- TypeScript
- Vue
- Vuex

The following resources have been invaluable:

- [Vue Class](https://class-component.vuejs.org/guide/class-component.html#data) for TypeScript with Vue components
- [Vuex Docs](https://vuex.vuejs.org/) for...Vuex
- [Vuex Class](https://github.com/ktsn/vuex-class/blob/master/README.md) for using Vuex within components
- [TypeScript handbook](https://www.typescriptlang.org/docs/handbook/basic-types.html)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Component Options

| Prop                             | Type    | Default                                                              | ACF Match                            |
| -------------------------------- | ------- | -------------------------------------------------------------------- | ------------------------------------ |
| findDealerText                   | string  | "Find a Dealer in Your Area"                                         | start_heading                        |
| startSearchPrompt                | string  | "Start by entering your zip code, city and state, or country below." | start_text                           |
| yourLocationText                 | string  | "Your location"                                                      | your_location_text                   |
| titleText                        | string  | "Where To Buy"                                                       | main_heading                         |
| loadingResultsText               | string  | "Loading results"                                                    | loading_results_text                 |
| searchLabel                      | string  | "Location"                                                           | zip_code_instructions                |
| locationResultsPlural            | string  | "Results for"                                                        | location_results_plural_label        |
| locationResultsSingular          | string  | "Result for"                                                         | location_results_singular_label      |
| filterToggleMobileLabel          | string  | "Filters"                                                            | refine_heading_mobile                |
| filterCancelLabel                | string  | "Cancel"                                                             | filter_cancel_label                  |
| filterApplyLabel                 | string  | "Apply"                                                              | filter_apply_label                   |
| typesLabel                       | string  | "Types"                                                              | types_heading                        |
| marketsLabel                     | string  | "Markets"                                                            | categories_heading                   |
| cropsLabel                       | string  | "Crops"                                                              | crops_heading                        |
| typesViewAllLabel                | string  | "See all types"                                                      | types_view_all_label                 |
| marketsViewAllLabel              | string  | "See all markets"                                                    | categories_view_all_label            |
| cropsViewAllLabel                | string  | "See all crops"                                                      | crops_view_all_label                 |
| filtersInitallyVisible           | integer | 6                                                                    | filters_initially_visible            |
| resetLabel                       | string  | "reset"                                                              | reset_text                           |
| dealerResultsSearchLabel         | string  | "Search"                                                             | dealer_results_search_label          |
| dealerResultsSearchPlaceholder   | string  | "I'm looking for ..."                                                | dealer_results_search_placeholder    |
| dealerResultsSortLabel           | string  | "Sort"                                                               | dealer_results_sort_label            |
| dealerResultsSortAscendingLabel  | string  | "A - Z"                                                              | dealer_results_sort_ascending_label  |
| dealerResultsSortDescendingLabel | string  | "Z - A"                                                              | dealer_results_sort_descending_label |
| dealerResultsPlural              | string  | "Results"                                                            | dealer_results_plural_label          |
| dealerResultsSingular            | string  | "Result"                                                             | dealer_results_singular_label        |
| websiteQuerySuffix               | string  | "utm_source=seminis_dealer_locator"                                  | website_query_suffix                 |
| language                         | string  | "global"                                                             | language                             |

## Deploying

To deploy this project, you'll want to:

- update the changelog
- update the version within the package.json
- push a git tag matching the version in the package.json up
- reference the updated version in the places that should use the newest dealer locator Vue app
  - Seminis WordPress Websites (seminis repo)
  - Seminis Where to Buy Laravel App (seminis-where)
- install the updated package in the aformentioned places
