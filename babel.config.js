module.exports = {
  presets: [
    [
      "@vue/cli-plugin-babel/preset",
      {
        useBuiltIns: "entry",
        corejs: {
          version: 3,
          proposals: true
        },
        targets: {
          edge: "85",
          ie: "11",
          firefox: "81",
          chrome: "86"
        }
      }
    ]
  ],
  plugins: [
    "@babel/plugin-transform-classes",
    "@babel/plugin-proposal-class-properties"
  ]
};
