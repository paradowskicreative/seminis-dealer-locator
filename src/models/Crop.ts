export default interface Crop {
  id: number;
  name: string;
  translations?: {
    [countryCode: string]: string;
  };
}
