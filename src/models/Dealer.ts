import Filter from "./Filter";

export default interface Dealer {
  id: number;
  name: string;
  types: Filter[];
  markets: Filter[];
  crops: Filter[];
  regions: Filter[];
  street_address: string;
  city: string;
  region: string;
  postal_code: string;
  country: string;
  website: string;
  email_address: string;
  job_title: string;
  mobile_phone: string;
  fax: string;
  office_phone: string;
  bio: string;
  notes: string;
}
