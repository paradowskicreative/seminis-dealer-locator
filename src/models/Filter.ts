export default interface Filter {
  id: number;
  name: string;
}
