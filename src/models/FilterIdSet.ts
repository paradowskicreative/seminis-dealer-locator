export default interface FilterIdSet {
  crops: number[];
  markets: number[];
  types: number[];
}
