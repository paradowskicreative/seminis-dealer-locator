import Filter from "./Filter";

export default interface FilterSet {
  crops: Filter[];
  markets: Filter[];
  types: Filter[];
}
