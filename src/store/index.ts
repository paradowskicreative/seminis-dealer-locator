import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import Fuse from "fuse.js";
import Dealer from "../models/Dealer";
import LatLng from "../models/LatLng";
import Filter from "@/models/Filter";
import FilterIdSet from "@/models/FilterIdSet";

Vue.use(Vuex);

const initialFilters = () => ({
  types: [] as number[],
  markets: [] as number[],
  crops: [] as number[]
});

export enum SearchType {
  SearchString,
  Location
}

export enum SortOrder {
  Ascending,
  Descending
}

export default new Vuex.Store({
  state: {
    introduced: false,
    searchString: "",
    currentSearchString: "",
    searchType: SearchType.SearchString,
    sortOrder: SortOrder.Ascending,
    location: {},
    dealers: Array<Dealer>(),
    dealerApiEndpoint: "",
    loading: false,
    error: "",
    filters: initialFilters(),
    resultsSearchString: "",
    googleMapsApiReady: false,
    language: ""
  },
  mutations: {
    setSearchString(state, payload: string): void {
      state.searchString = payload;
      if (!state.introduced) state.introduced = true;
    },
    setCurrentSearchString(state, payload: string): void {
      state.currentSearchString = payload;
    },
    setSearchType(state, payload: SearchType) {
      state.searchType = payload;
    },
    setSortOrder(state, payload: SortOrder) {
      state.sortOrder = payload;
    },
    setLocation(state, payload: LatLng): void {
      state.location = payload;
    },
    setLanguage(state, payload: string): void {
      state.language = payload;
    },
    setIntroduced(state, payload: boolean): void {
      state.introduced = payload;
    },
    setDealers(state, payload: Dealer[]): void {
      state.dealers = payload;
    },
    setLoading(state, payload: boolean): void {
      state.loading = payload;
    },
    setError(state, payload: string): void {
      state.error = payload;
    },
    setFilters(state, payload: FilterIdSet): void {
      state.filters = payload;
    },
    setResultsSearchString(state, payload: string): void {
      state.resultsSearchString = payload;
    },
    setDealerApiEndpoint(state, payload: string): void {
      state.dealerApiEndpoint = payload;
    },
    setGoogleMapsApiReady(state, payload: boolean) {
      state.googleMapsApiReady = payload;
    }
  },
  actions: {
    fetchDealers({ state, commit }) {
      commit("setError", "");
      commit("setLoading", true);
      if (!state.introduced) commit("setIntroduced", true);
      if (state.searchType == SearchType.SearchString) {
        commit("setCurrentSearchString", state.searchString);
      } else {
        commit("setCurrentSearchString", "");
      }
      commit("setResultsSearchString", "");
      axios
        .post(state.dealerApiEndpoint, {
          location: state.location
        })
        .then(response => {
          commit("setDealers", response.data);
        })
        .catch(error => {
          commit("setError", error);
          // eslint-disable-next-line
          console.warn(error);
        })
        .finally(() => {
          commit("setLoading", false);
        });
    }
  },
  getters: {
    types({ dealers }): Filter[] {
      const targetKey = "types";
      return dealers
        .reduce(
          (carry, current) =>
            carry.concat(
              current[targetKey].filter(
                (model: Filter) =>
                  undefined ===
                  carry.find((search: Filter) => search.id === model.id)
              )
            ),

          [] as Filter[]
        )
        .sort((a: Filter, b: Filter) => (a.name < b.name ? -1 : 1));
    },
    markets({ dealers }): Filter[] {
      const targetKey = "markets";
      return dealers
        .reduce(
          (carry, current) =>
            carry.concat(
              current[targetKey].filter(
                (model: Filter) =>
                  undefined ===
                  carry.find((search: Filter) => search.id === model.id)
              )
            ),

          [] as Filter[]
        )
        .sort((a: Filter, b: Filter) => (a.name < b.name ? -1 : 1));
    },
    crops({ dealers }): Filter[] {
      const targetKey = "crops";
      return dealers
        .reduce(
          (carry, current) =>
            carry.concat(
              current[targetKey].filter(
                (model: Filter) =>
                  undefined ===
                  carry.find((search: Filter) => search.id === model.id)
              )
            ),

          [] as Filter[]
        )
        .sort((a: Filter, b: Filter) => (a.name < b.name ? -1 : 1));
    },
    filteredDealers({
      dealers,
      filters,
      resultsSearchString,
      sortOrder
    }): Dealer[] {
      const filteredDealers = dealers.reduce(
        (carry: Dealer[], current: Dealer) => {
          if (
            filters.types.length > 0 &&
            filters.types.filter(
              (id: number) =>
                undefined !==
                current.types.find((filter: Filter) => filter.id === id)
            ).length === 0
          ) {
            return carry;
          }
          if (
            filters.markets.length > 0 &&
            filters.markets.filter(
              (id: number) =>
                undefined !==
                current.markets.find((filter: Filter) => filter.id === id)
            ).length === 0
          ) {
            return carry;
          }
          if (
            filters.crops.length > 0 &&
            filters.crops.filter(
              (id: number) =>
                undefined !==
                current.crops.find((filter: Filter) => filter.id === id)
            ).length === 0
          ) {
            return carry;
          }

          carry.push(current);

          return carry;
        },
        [] as Dealer[]
      );

      if (resultsSearchString.length === 0) {
        return filteredDealers.sort((a: Dealer, b: Dealer) => {
          if (sortOrder === SortOrder.Ascending) {
            return a.name < b.name ? -1 : 1;
          } else {
            return a.name > b.name ? -1 : 1;
          }
        });
      }

      const options = {
        keys: [
          "name",
          "street_address",
          "city",
          "region",
          "country",
          "email_address",
          "website"
        ],
        distance: 500
      };

      const fuse = new Fuse(filteredDealers, options);

      return fuse.search(resultsSearchString).map(result => result.item);
    }
  }
});
