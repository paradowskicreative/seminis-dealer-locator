module.exports = {
  css: {
    extract: false
  },
  transpileDependencies: [
    "fuse.js"
  ]
};
